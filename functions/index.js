const functions = require('firebase-functions');
const admin     = require('firebase-admin');
admin.initializeApp();
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const QTD_QUESTOES = 10;

const db = admin.database();

const questoesRef  = db.ref("questoes");

const turmasRef    = db.ref("turmas");

// Estrutura: simulados/turmaid/simuladoid/
const simuladosRef = db.ref("simulados");

// Insere um novo simulado no banco.
// Recebe o tid, nome do simulado, nome da turma e qtd de questões.
// Ex: http://localhost:5001/iamulado-79e4c/us-central1/gerarSimulado?tid=-LCo02wESSitkyy2FpYG&nomeSimulado=Simulado A01&nomeTurma=Turma A
exports.gerarSimulado = functions.https.onRequest((request, response) => 
{
    let tid = request.query.tid;
    let sid = "";

    let qtdQuestoes = request.query.qtd;
     dificuldade = request.query.dificuldade;

    let simulado = {};

    simulado.nome      = request.query.nomeSimulado;
    simulado.nomeTurma = request.query.nomeTurma;

    getQuestoes(QTD_QUESTOES, dificuldade).then(questoes => 
    {
        simulado.questoes = questoes;

        let simuladoRef = simuladosRef.child(tid).push();
        sid = simuladoRef.key;

        console.log(sid);

        simuladoRef.set(simulado);

        return resolverSimulado(tid, sid);
    }).then(p => {
        response.send("Simulado '" + simulado.nome + "' cadastrado e realizado com sucesso");
        
        return true;
    }).then(t => {
        return ajustarDificuldadeQuestoes(tid, sid);
    }).catch(err => {
        console.log(err);
    });
});

// Cadastrar a resolução de um simulado de todos os alunos da turma
// Recebe o tid e sid.
// Ex: http://localhost:5001/iamulado-79e4c/us-central1/resolverSimulado?tid=-LCo02wESSitkyy2FpYG&sid=-LCoXHxgmfMStZ7k_ikM
exports.resolverSimulado = functions.https.onRequest((request, response) => 
{
    let tid = request.query.tid;
    let sid = request.query.sid;

    resolverSimulado(tid, sid).then(p => {
        response.send("Simulado resolvido");
        return true;
    }).catch(err => {
        console.log(err);
    });

    /*let simuladoRef     = simuladosRef.child(tid).child(sid);
    let questoesSimuRef = simuladoRef.child('questoes'); 
    let alunosRef       = turmasRef.child(tid).child('alunos');

    // Pegar os dados do simulado
    simuladoRef.once('value').then(snapshot => {
        return snapshot.val().questoes;
    }).then(questoes => 
    {
        // Pegar os dados dos alunos
        alunosRef.once('value', snapshot => 
        {
            let questoesPromise = [];
            
            for(aluno in snapshot.val())
            {
                for(questao in questoes)
                {
                    let resolucaoRef = questoesSimuRef.child(questao).child('resolucoes');

                    let acertou = Math.random() <= 0.5 ? false : true;

                    // alunouid : acertou (true/false)
                    questoesPromise.push(resolucaoRef.child(aluno).set(acertou));
                }
            }

            Promise.all(questoesPromise).then(function(){
                response.send("Simulado resolvido");
            });
        });
    })*/
});

function resolverSimulado(tid, sid)
{
    let simuladoRef     = simuladosRef.child(tid).child(sid);
    let questoesSimuRef = simuladoRef.child('questoes'); 
    let alunosRef       = turmasRef.child(tid).child('alunos');

    // Pegar os dados do simulado
    return simuladoRef.once('value').then(snapshot => {
        return snapshot.val().questoes;
    }).then(questoes => 
    {
        // Pegar os dados dos alunos
        return alunosRef.once('value', snapshot => 
        {
            let questoesPromise = [];
            
            for(aluno in snapshot.val())
            {
                for(questao in questoes)
                {
                    let resolucaoRef = questoesSimuRef.child(questao).child('resolucoes');

                    let acertou = Math.random() <= 0.5 ? false : true;

                    // alunouid : acertou (true/false)
                    questoesPromise.push(resolucaoRef.child(aluno).set(acertou));
                }
            }

            return Promise.all(questoesPromise);
        });
    });
}

// Cadastrar um número x de questões
// Ex: http://localhost:5001/iamulado-79e4c/us-central1/gerarQuestoes?qtd=120
exports.gerarQuestoes = functions.https.onRequest((request, response) =>
{
    let qtd = request.query.qtd;
    let promises = [];

    for(i = 0; i < qtd; i++)
    {
        let questao = {};
        questao.texto = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut hendrerit efficitur imperdiet. Vestibulum sit amet sapien non felis mollis pharetra. Proin mauris arcu, facilisis eu nisi eget, laoreet pharetra orci. Aenean sodales nec elit sed imperdiet. Cras feugiat aliquam tempor. Vivamus dolor neque, facilisis vel pharetra ac, placerat nec nisi. Mauris eget tincidunt arcu.";

        questao.alternativas = {
            'Alternativa 1' : true,
            'Alternativa 2' : false,
            'Alternativa 3' : false,
            'Alternativa 4' : false,
            'Alternativa 5' : false
        };

        questao.dificuldade = Math.floor((Math.random() * 100) + 1);
        
        let promise = questoesRef.push().set(questao);
    }

    Promise.all(promises).then(() => {
        return response.send(qtd + " questões cadastradas");
    }).catch((err) => {
        console.log(err);
    });
});

// Fiz a função pra testar direto no browser já e ver como que tava o JSON
// http://localhost:5001/iamulado-79e4c/us-central1/getQuestoes?qtd=10&dificuldade=50
exports.getQuestoes = functions.https.onRequest((request, response) =>
{
    let qtd = request.query.qtd;
    let dificuldade = request.query.dificuldade;

    getQuestoes(qtd, dificuldade).then(questoes => {
        response.send(questoes);
    });
});

// Pegar questões do banco
// https://stackoverflow.com/questions/33893866/orderbychild-not-working-in-firebase
function getQuestoes(qtd, dificuldade)
{
    return questoesRef.once('value').then(snapshot => 
    {
        let questoes = [];
        let questao = {};

        snapshot.forEach(child => 
        {
            questao    = child.val();
            questao.id = child.key;

            questoes.push(questao);
        });

        // https://stackoverflow.com/questions/26922131/sorting-an-array-by-which-value-is-closest-to-1
        // Vai ordenar de acordo com as questões com dificuldade mais próximas da dificuldade informada.
        questoes = questoes.sort(function(a, b) { 
            return Math.abs(dificuldade - a.dificuldade) - Math.abs(dificuldade - b.dificuldade)
        });

        return questoes.slice(0, qtd);
    });
}

// Toda vez que um simulado novo for realizado, ajustar a dificuldade das questões.
function ajustarDificuldadeQuestoes(tid, sid)
{
    let simuladoRef = simuladosRef.child(tid).child(sid);

    simuladoRef.once('value', snapshot => 
    {
        let simulado = snapshot.val();
        let questoes = simulado.questoes;

        questoes.forEach(questao => 
        {
            qtd_resolucoes = 0;
            qtd_acertos    = 0;

            for(chave in questao.resolucoes)
            {
                resolucao = questao.resolucoes[chave];

                console.log(resolucao);

                resolucao ? qtd_acertos++ : null;

                qtd_resolucoes++;
            }

            let media = (qtd_acertos / qtd_resolucoes * 100);
            
            atualizarDificuldadeQuestao(questao, media);
        });
    });
}

function atualizarDificuldadeQuestao(questao, media)
{
    let novaDificuldade = 100 - media;

    questoesRef.child(questao.id).child("dificuldade").set(novaDificuldade);
}