# FUZZYMULADO

![Thinking](https://vignette.wikia.nocookie.net/cardfight/images/1/15/Ultimate_thonking.gif/revision/latest/scale-to-width-down/480?cb=20171213040245)

Antes de começar, certifique-se de ter o [NodeJS](https://nodejs.org/en/) (Para instalar o Bower com o npm e caso a gente for usar functions) e o [bower](https://bower.io/) (Gerenciador de pacotes para o front-end) instalados na sua máquina.

Depois, entre na pasta functions e digite o comando:

```bash
$ npm install
```

Em seguida, vá para a pasta public e faça:

```bash
$ bower install
```

Não se esqueça de instalar as firebase tools e logar na sua conta

```bash
$ npm install -g firebase-tools
$ firebase login
```

Feito isso, você pode testar o site localmente com o comando na raiz:

```bash
$ firebase serve
```

Teste o site somente com esse comando. Se simplesmente abrir o index.html no seco, por exemplo, ele vai dar erro que não tá conseguindo localizar os arquivos do firebase.

Agora, se quiser mandar sua versão para o Firebase Hosting, é só fazer

```bash
$ firebase deploy
```
