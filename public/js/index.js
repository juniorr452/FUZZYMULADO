const SENHA = "123456";

$(document).ready(function()
{
    let auth = firebase.auth();

    // Inicializar os selects
    $('select').formSelect();

    $('#form_cadastrar').submit(function() 
    { 
        let dados = $('#form_cadastrar').serializeJSON();
        
        auth.createUserWithEmailAndPassword(dados.email, SENHA)
        .then(function()
        {
            let user = auth.currentUser;

            firebase.database().ref("usuarios").child(user.uid).set(dados).then(entrar);
        })
        .catch(error => {
            M.toast({html: error.message});
        });

        return false;
    });

    $('#form_entrar').submit(e =>
    { 
        let dados = $('#form_entrar').serializeJSON();

        auth.signInWithEmailAndPassword(dados.email, SENHA)
        .then(entrar)
        .catch(error => {
            M.toast({html: error.message});
        });
        
        return false;
    });
});

function entrar(){
    document.location.href = "/inicio.html";
}