let uid = firebase.auth().currentUser.uid;

let turmasRef       = db.ref("turmas");
let alunoTurmasRef  = db.ref("usuarios").child(uid).child("turmas");

let html;

alunoTurmasRef.on('child_added', snapshot => 
{
    let turma = snapshot.val();

    html  = "<tr>";
    html +=     "<td>" + turma.nome + "</td>";
    html +=     "<td>" + turma.professor + "</td>";
    html +=     "<td>" + criarBotaoSair(snapshot.key) + "</td>";
    html += "</tr>";
    
    $('#tbody_aluno_turmas').append(html);
});

function criarBotaoSair(tid)
{
    let btn = document.createElement("a");

    btn.className = "waves-effect waves-light btn red";
    btn.text      = "Sair";
    
    btn.setAttribute("onclick", "desmatricularAluno('" + tid + "')");

    return btn.outerHTML;
}

function desmatricularAluno(tid)
{
    turmasRef.child(tid).child("alunos").child(uid).remove();
    alunoTurmasRef.child(tid).remove();

    M.toast({html:"Desmatriculado com sucesso!"});
}