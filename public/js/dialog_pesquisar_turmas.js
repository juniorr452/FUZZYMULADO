$('#nome_turma').on("input", function()
{
    let nomeTurma = $('#nome_turma').val();
    let html      = "";

    turmasRef.orderByChild("nome").startAt(nomeTurma).once('value', snapshot => 
    {
        snapshot.forEach(turma => 
        {
            let t = turma.val();

            html += "<tr>";
            html +=     "<td>" + t.nome + "</td>";
            html +=     "<td>" + t.professor + "</td>";
            html +=     "<td>" + criarBotaoEntrar(turma) + "</td>";
            html += "</tr>";
        });

        $("#tbody_turma").html(html);
    });
})

function criarBotaoEntrar(turma)
{
    let btn = document.createElement("a");

    btn.className = "waves-effect waves-light btn green";
    btn.text      = "Entrar";

    // Por algum motivo ele não me deixa usar o onclick normal, não consegui encontrar o porquê
    btn.setAttribute("onclick", "matricularAluno('" + JSON.stringify(turma) + "', '" + turma.key + "')");

    return btn.outerHTML;
}

function matricularAluno(t, tid)
{
    let turma = JSON.parse(t);
    delete turma.alunos;

    let alunoRef  = db.ref("usuarios").child(uid);

    turmasRef.child(tid).child("alunos").child(uid).set(true);
    alunoRef.child("turmas").child(tid).set(turma);

    console.log(turma);

    M.toast({html:"Cadastro na turma realizado com sucesso!"});
}