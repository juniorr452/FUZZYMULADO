let auth = firebase.auth();
let db   = firebase.database();

$(document).ready(function()
{
    inicializarMaterialize();

    $('#btn_nova_turma').click(function()
    {
        $('#modal_titulo').html('Nova Turma');

        abrirModal("dialog_nova_turma.html")
    });

    $('#btn_nova_questao').click(function(){
        $('#modal_titulo').html('Nova Questão');
    });

    $('#btn_nova_prova').click(function(){
        $('#modal_titulo').html('Nova Prova');

        abrirModal("dialog_nova_prova.html")
        
    });

    $('#btn_pesquisar_turmas').click(function()
    {
        $('#modal_titulo').html('Pesquisar Turmas');

        abrirModal("dialog_pesquisar_turmas.html");
    });

    $('#btn_sair').click(function(){
        auth.signOut();
    });

    auth.onAuthStateChanged(user => 
    {
        if(user)  
        {
            M.toast({html : "Logado como " + user.email});

            db.ref('usuarios').child(user.uid).once('value').then(dataSnapshot => {
                carregarPagina(dataSnapshot.val().posicao);
            });
        }
    });

    function abrirModal(htmlUrl)
    {
        $.ajax({
            url: htmlUrl,
            success: function (response) 
            {
                $('#modal_conteudo').html(response);
                $('#modal').modal('open');
            }
        });
    }

    function carregarPagina(posicao)
    {
        switch(posicao)
        {
            case "professor":
                /*$.when(
                    $.ajax({
                        url: "tabela_professor_simulados.html",
                        dataType: "html"
                    }),
                    $.ajax({
                        url: "tabela_professor_turmas.html",
                        dataType: "html"
                    })
                )
                .then((r1, r2) => {
                    $('#conteudo').html(r1 + r2);
                });*/

                $.ajax({
                    url: "tabela_professor_simulados.html",
                    success: function (response) {
                        $('#conteudo').append(response);
                        $('#preloader').hide();
                    }
                });

                $.ajax({
                    url: "tabela_professor_turmas.html",
                    success: function (response) {
                        $('#conteudo').append(response);
                        $('#preloader').hide();
                    }
                });

                $('#fab_professor').show();
            break;

            case "aluno":
                /*$.when(
                    $.ajax({
                        url: "tabela_aluno_turmas.html"
                    })
                )
                .then((r1) => {
                    $('#conteudo').html(r1);
                });*/

                $.ajax({
                    url: "tabela_aluno_turmas.html",
                    success: function (response) {
                        $('#conteudo').html(response);
                    }
                });

                $('#fab_aluno').show();
            break;
        }
    }

    function inicializarMaterialize()
    {
        // Inicializar o FAB
        $('.fixed-action-btn').floatingActionButton();

        // Inicializar Tooltips
        $('.tooltipped').tooltip();

        // Inicializar o modal
        $('.modal').modal();
    }
});