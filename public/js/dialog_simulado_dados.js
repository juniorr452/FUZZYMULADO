simuladosRef = firebase.database().ref('simulados').child(dadosSimulado.tid).child(dadosSimulado.sid);

simuladosRef.once('value', snapshot => 
{
    simulado = snapshot.val()

    let tbody = "";
    let questao_numero = 1;

    let qtd_acertos    = 0;
    let qtd_resolucoes = 0;

/*     Object.values(simulado.questoes).forEach(questao => {
        tbody += '<tr>';
        tbody +=    '<td>' + questao_numero + '</td>';
        console.log(questao);
        
        tbody +=    '<td>' + questao.dificuldade + '</td>';

        qtd_resolucoes = 0;
        qtd_acertos    = 0;
        Object.values(questao.resolucoes).forEach(resolucao => {
            resolucao ? qtd_acertos++ : null;

            qtd_resolucoes++;
        });

        tbody +=    '<td>' + (qtd_acertos / qtd_resolucoes * 100) + '%</td>';
        tbody += '</tr>';

        questao_numero++;
    }); */

    Object.keys(simulado.questoes).forEach(questao_chave => {
        questao = simulado.questoes[questao_chave];
        tbody += '<tr>';
        tbody +=    '<td>' + questao.id + '</td>';
        console.log(questao);

        tbody += '<td>' + questao.dificuldade + '</td>';

        qtd_resolucoes = 0;
        qtd_acertos    = 0;
        Object.values(questao.resolucoes).forEach(resolucao => {
            resolucao ? qtd_acertos++ : null;

            qtd_resolucoes++;
        });

        tbody +=    '<td>' + (qtd_acertos / qtd_resolucoes * 100) + '%</td>';
        tbody += '</tr>';

        questao_numero++;
    });

    $('#tbody_questoes_simulado').html(tbody);
});