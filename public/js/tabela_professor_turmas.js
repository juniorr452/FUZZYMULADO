turmasRef = firebase.database().ref("turmas");

turmasRef.orderByChild('professor').equalTo(email).on('child_added', snapshot => {
    let turma = snapshot.val();
    let html  = "";

    html += "<tr>";
    html +=     "<td>" + turma.nome + "</td>";
    html +=     "<td>" + (turma.alunos ? Object.keys(turma.alunos).length : "0") + "</td>";
    html += "</tr>";
    
    $('#tbody_professor_turmas').append(html);
});