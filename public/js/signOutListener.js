// Redirecionar para a página inicial, caso não esteja logado
firebase.auth().onAuthStateChanged(user => {
    if(!user)
        document.location.href="/";
});
