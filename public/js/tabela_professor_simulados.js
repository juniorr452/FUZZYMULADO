let turmasRef = firebase.database().ref("turmas");
let email = firebase.auth().currentUser.email;
let simulado;

turmasRef.orderByChild('professor').equalTo(email).on('child_added', snapshot => 
{
    let turma = snapshot.val();

    let simuladosRef = firebase.database().ref('simulados').child(snapshot.key).on('child_added', snapshot2 => 
    {
        let simu = snapshot2.val();
        let html  = "";

        let tid = "'" + snapshot.key + "'";
        let sid = "'" + snapshot2.key + "'";
        let parametros = tid + "," + sid;

        html += "<tr onclick=abrirModalSimulado(" + parametros + ")>";
        html +=     "<td>" + simu.nome  + "</td>";
        html +=     "<td>" + turma.nome + "</td>";
        html += "</tr>";

        $('#tbody_professor_simulados').append(html);
    });
});

let dadosSimulado = {};

function abrirModalSimulado(turmaid, simuid)
{
    $('#modal_titulo').html('Dados do simulado');

    dadosSimulado = {
        tid: turmaid,
        sid: simuid
    };

    $.ajax({
        url: "dialog_simulado_dados.html",
        success: function (response) 
        {
            $('#modal_conteudo').html(response);
            $('#modal').modal('open');
        }
    });
}