uid = firebase.auth().currentUser.uid;

//let turmasRef       = db.ref("turmas");
let html;

const GERAR_SIMULADO_URL = "http://localhost:5001/iamulado-79e4c/us-central1/gerarSimulado";

turmasRef.orderByChild('professor').equalTo(email).on('child_added', snapshot => {
    let turma = snapshot.val();
    let html  = "";

    html += "<option value='" + snapshot.key + "'>" + turma.nome + "</option>";
    
    $('#select_turma').append(html);
});

$('select').formSelect();

$('#btnCriarProva').click(btn => {

    var turmaid = $("#select_turma").val();
    var simuladoNome = $("#nome_simulado").val();
    var dificuldade = $("#slider").val();

    $.ajax({
        url: GERAR_SIMULADO_URL,
        type: "get",
        data: 
        {
            tid: turmaid,
            nomeSimulado: simuladoNome,
            nomeTurma: "Turmaaaa",
            dificuldade: dificuldade
        },
        success: function(response) 
        {
            console.log(response);
            $('#modal').modal('close');
        }
    });
})

function criarBotaoSair(tid)
{
    let btn = document.createElement("a");

    btn.className = "waves-effect waves-light btn red";
    btn.text      = "Sair";
    
    btn.setAttribute("onclick", "desmatricularAluno('" + tid + "')");

    return btn.outerHTML;
}

function desmatricularAluno(tid)
{
    turmasRef.child(tid).child("alunos").child(uid).remove();
    alunoTurmasRef.child(tid).remove();

    M.toast({html:"Desmatriculado com sucesso!"});
}

        