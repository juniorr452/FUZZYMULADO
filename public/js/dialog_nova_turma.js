$('#form_turma').submit(e =>
{
    user       = firebase.auth().currentUser;
    turmasRef  = firebase.database().ref("turmas");

    dadosTurma = $('#form_turma').serializeJSON();
    dadosTurma.professor = user.email;

    turmasRef.push().set(dadosTurma)
    .then(function(){
        M.toast({html : "Turma criada com sucesso!"});

        $('#nome_turma').val('');
    })
    .catch(error => {
        M.toast({html : error.message});
    });

    return false;
});